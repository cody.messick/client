.. Copyright (C) Alexander Pace (2021)
.. Copyright (C) Duncan Meacher (2021)

igwn-alert Documentation
============================

igwn-alert is a client for the LIGO/Virgo/KAGRA alert infrastructure 
that is powered by `Apache Kafka`_. igwn-alert uses a publish-subcsribe 
(pubsub) model to alert users and follow-up processes to state 
changes in GraceDB. It is a high-level extension of the generalized
`hop-client`_ pubsub tool intended as a replacement of the legacy XMPP
`LVAlert`_ service.

Please visit the `igwn-alert User Guide`_ for a brief overview and starter for the
igwn-alert client code and service. 

Existing LVAlert Users are encouraged to read the
`LVAlert to igwn-alert Transition Guide <lvalert_to_igwnalert.html>`_ to learn 
the latest on the rollout schedule and to see the differences between
LVAlert and igwn-alert.

Users with questions can email computing-help@igwn.org. LVK members can
reach out for support on the `igwn-alert Mattermost channel`_.


.. toctree::
    :maxdepth: 1
    :caption: Contents:

    quickstart
    guide
    lvalert_to_igwnalert
    api
    cli


.. _Apache Kafka: https://kafka.apache.org/
.. _hop-client: https://hop-client.readthedocs.io/
.. _igwn-alert User Guide: guide.html
.. _LVAlert: https://ligo-lvalert.readthedocs.io/en/latest/
.. _computing-help@igwn.org:: mailto:computing-help@igwn.org
.. _igwn-alert Mattermost channel: https://chat.ligo.org/ligo/channels/igwn-alert
